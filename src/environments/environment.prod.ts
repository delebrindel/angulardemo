export const environment = {
  production: true,
  menu: [{ name: 'Magazines', link: './magazines' }],
  API: 'https://api.stokedonfixedbikes.com/api/magazines'
};
