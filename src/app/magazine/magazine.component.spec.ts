import { RouterTestingModule } from '@angular/router/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MagazineComponent } from './magazine.component';

describe('MagazineComponent', () => {
  let component: MagazineComponent;
  let fixture: ComponentFixture<MagazineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MagazineComponent],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MagazineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit an event when clicked', () => {
    component.item = { id: 1, img: 'https://placehold.it/350' };
    component.isDetail = true;
    fixture.detectChanges();
    spyOn(component.propagar, 'emit').and.callThrough();
    const btn = fixture.nativeElement.querySelector('.boton-propagar');
    btn.click();
    fixture.whenStable().then(() => {
      expect(component.propagar.emit).toHaveBeenCalled();
    });
  });

  it('should show detail button if component is detail', () => {
    component.item = { id: 1, img: 'https://placehold.it/350' };
    component.isDetail = true;
    fixture.detectChanges();
    const btn = fixture.nativeElement.querySelector('.boton-propagar');
    expect(btn).toBeTruthy();
  });

  it('should not show detail button if component is not detail', () => {
    component.item = { id: 1, img: 'https://placehold.it/350' };
    component.isDetail = false;
    fixture.detectChanges();
    const btn = fixture.nativeElement.querySelector('.boton-propagar');
    expect(btn).toBeFalsy();
  });
});
