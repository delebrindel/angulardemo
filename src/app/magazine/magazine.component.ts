import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-magazine',
  templateUrl: './magazine.component.html',
  styleUrls: ['./magazine.component.css']
})
export class MagazineComponent implements OnInit {
  @Input() item;
  @Input() isDetail: Boolean = false;
  @Output() propagar = new EventEmitter();

  constructor() {}

  ngOnInit() {}
  onPropagar() {
    console.log('Propagando');
    this.propagar.emit('Me propago');
  }
}
