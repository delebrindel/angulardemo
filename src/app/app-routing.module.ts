import { QuesoComponent } from './queso/queso.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'queso',
    component: QuesoComponent
  },
  {
    path: 'magazines',
    loadChildren: () => import('./magazines/magazines.module').then(m => m.MagazinesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
