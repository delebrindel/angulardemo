import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should have title "home"', () => {
    const title = fixture.nativeElement.querySelector('h1');
    expect(title.innerHTML).toBe('Home');
  });
  it('should have subtitle "Welcome to biking party"', () => {
    const subtitle = fixture.nativeElement.querySelector('h2');
    expect(subtitle.innerHTML).toBe('Welcome to biking party');
  });
});
