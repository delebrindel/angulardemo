import { fakeAsync, inject, TestBed } from '@angular/core/testing';
import { MagazinesServiceService } from './magazines-service.service';
import { HttpClientModule } from '@angular/common/http';
import { MagazinesModule } from '../magazines/magazines.module';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from '../../environments/environment';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
describe('MagazinesServiceService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    })
  );
  it('should be created', () => {
    const service: MagazinesServiceService = TestBed.get(MagazinesServiceService);
    expect(service).toBeTruthy();
  });
  it(
    'should getMagazines call ' + environment.API,
    fakeAsync(
      inject(
        [MagazinesServiceService, HttpTestingController],
        (service: MagazinesServiceService, backend: HttpTestingController) => {
          expect(service.getMagazines).toBeDefined();
          service.getMagazines().subscribe(() => {
            let url;
            backend.match(request => {
              url = request.urlWithParams;
              expect(request.url).toBe(environment.API);
              return request.url === environment.API;
            });
            backend.expectOne(url);
            backend.verify();
          });
        }
      )
    )
  );
});
