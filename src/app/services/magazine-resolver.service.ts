import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { MagazinesServiceService } from './magazines-service.service';
@Injectable({
  providedIn: 'root'
})
export class MagazineResolverService implements Resolve<any> {
  constructor(private magazinesService: MagazinesServiceService) {}
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> {
    return this.magazinesService.getMagazineById(route.params['id']);
  }
}
