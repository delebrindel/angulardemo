import { Magazine } from './../models/magazine';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { observable, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MagazinesServiceService {
  constructor(private http: HttpClient) {}
  getMagazines(): Observable<Magazine[]> {
    return this.http.get<Magazine[]>(environment.API);
  }
  getMagazineById(id: string | number): Observable<Magazine> {
    return this.http.get<Magazine[]>(environment.API + '/' + id);
  }
}
