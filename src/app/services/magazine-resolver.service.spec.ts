import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { MagazineResolverService } from './magazine-resolver.service';

describe('MagazineResolverService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    })
  );

  it('should be created', () => {
    const service: MagazineResolverService = TestBed.get(MagazineResolverService);
    expect(service).toBeTruthy();
  });
});
