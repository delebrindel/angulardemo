import { MagazineResolverService } from './../services/magazine-resolver.service';
import { MagazinesServiceService } from './../services/magazines-service.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MagazinesRoutingModule } from './magazines-routing.module';
import { MagazinesListComponent } from './magazines-list/magazines-list.component';
import { MagazineDetailComponent } from './magazine-detail/magazine-detail.component';
import { MagazineComponent } from '../magazine/magazine.component';

@NgModule({
  declarations: [MagazinesListComponent, MagazineDetailComponent, MagazineComponent],
  imports: [CommonModule, MagazinesRoutingModule],
  providers: [MagazinesServiceService, MagazineResolverService]
})
export class MagazinesModule {}
