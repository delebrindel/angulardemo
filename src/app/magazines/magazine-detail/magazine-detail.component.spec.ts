import { RouterTestingModule } from '@angular/router/testing';
import { MagazineComponent } from './../../magazine/magazine.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MagazineDetailComponent } from './magazine-detail.component';

describe('MagazineDetailComponent', () => {
  let component: MagazineDetailComponent;
  let fixture: ComponentFixture<MagazineDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MagazineDetailComponent, MagazineComponent],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MagazineDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
