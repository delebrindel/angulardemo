import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-magazine-detail',
  templateUrl: './magazine-detail.component.html',
  styleUrls: ['./magazine-detail.component.css']
})
export class MagazineDetailComponent implements OnInit {
  magazine: {};
  constructor(private route: ActivatedRoute) {
    this.magazine = this.route.snapshot.data['magazine'] || {};
  }

  ngOnInit() {}

  onPropagar(datos) {
    console.log('Me mandaron ', datos);
  }
}
