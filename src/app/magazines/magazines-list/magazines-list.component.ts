import { Magazine } from './../../models/magazine';
import { MagazinesServiceService } from './../../services/magazines-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-magazines-list',
  templateUrl: './magazines-list.component.html',
  styleUrls: ['./magazines-list.component.css']
})
export class MagazinesListComponent implements OnInit {
  magazinesList: Magazine[];
  constructor(private magazinesService: MagazinesServiceService) {}

  ngOnInit() {
    this.magazinesService.getMagazines().subscribe(data => {
      this.magazinesList = data;
    });
  }
  openDetail(id) {
    //router navigate (id) obj
  }
  onPropagar(datos) {
    console.log(datos);
  }
}
