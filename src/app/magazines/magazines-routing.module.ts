import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MagazinesListComponent } from './magazines-list/magazines-list.component';
import { MagazineDetailComponent } from './magazine-detail/magazine-detail.component';
import { MagazineResolverService } from './../services/magazine-resolver.service';

const routes: Routes = [
  { path: '', component: MagazinesListComponent },
  { path: ':id', component: MagazineDetailComponent, resolve: { magazine: MagazineResolverService } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MagazinesRoutingModule {}
